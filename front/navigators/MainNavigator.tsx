import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import { SignInScreen, SignUpScreen } from "../screens";
import { MainStackParamList } from "../types/types";
import ConversationNavigator from "./ConversationNavigator";

const MainStack = createNativeStackNavigator<MainStackParamList>();

export default function MainNavigator() {
  return (
    <MainStack.Navigator screenOptions={{ headerShown: false }}>
      <MainStack.Screen
        name='SignUp'
        component={SignUpScreen}
        options={{
          headerTitleAlign: "center",
        }}
      />
      <MainStack.Screen
        name='SignIn'
        component={SignInScreen}
        options={{
          headerTitleAlign: "center",
        }}
      />
      <MainStack.Screen
        name='Conversation'
        component={ConversationNavigator}
        options={{
          headerTitleAlign: "center",
        }}
      />
    </MainStack.Navigator>
  );
}

import React from "react";
import { View, Text, Platform } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { FontAwesome, AntDesign } from "@expo/vector-icons";
import { ConversationScreen } from "../screens";

const Tab = createBottomTabNavigator();
export default function ConversationNavigator() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarActiveTintColor: "#1b88ff",
        tabBarInactiveTintColor: "#000",
        tabBarStyle: {
          height: Platform.OS === "ios" ? 90 : 70,
          paddingVertical: Platform.OS === "ios" ? 5 : 0,
        },
        tabBarItemStyle: { paddingVertical: Platform.OS === "ios" ? 0 : 8 },
        headerShown: false,
      })}
    >
      <Tab.Screen
        name='Chats'
        component={ConversationScreen}
        options={{
          tabBarLabel: ({ focused, color }) => (
            <Text
              style={{
                color: color,
                fontSize: 15,
                fontWeight: focused ? "700" : "500",
              }}
            >
              Chats
            </Text>
          ),
          tabBarIcon: ({ focused, color }: any) => (
            <AntDesign name='message1' color={color} size={25} />
          ),
        }}
      />
      <Tab.Screen
        name='Profil'
        component={FakeScreen}
        options={{
          tabBarLabel: ({ focused, color }) => (
            <Text
              style={{
                color: color,
                fontSize: 15,
                fontWeight: focused ? "700" : "500",
              }}
            >
              Profil
            </Text>
          ),
          tabBarIcon: ({ focused, color, size }: any) => (
            <FontAwesome name='user' color={color} size={25} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const FakeScreen = () => {
  return <View></View>;
};

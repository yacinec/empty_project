export type RootStackParamList = {
  Sign: undefined;
};

export type MainStackParamList = {
  SignUp: undefined;
  SignIn: undefined;
  Conversation: undefined;
};

export type SignInScreenProp = StackNavigationProp<
  MainStackParamList,
  "SignUp",
  "Conversation"
>;
export type SignUpScreenProp = StackNavigationProp<
  MainStackParamList,
  "SignIn",
  "Conversation"
>;

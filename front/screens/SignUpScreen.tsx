import React from "react";
import {
  View,
  ImageBackground,
  StyleSheet,
  KeyboardAvoidingView,
} from "react-native";
import Button from "../components/Button";
import Footer from "../components/Footer";
import Input from "../components/Input";
import Title from "../components/Title";
import { SignUpScreenProp } from "../types/types";
const signBg = require("../assets/bg.jpg");

export default function SignUpScreen({ navigation }: SignUpScreenProp) {
  return (
    <KeyboardAvoidingView>
      <ImageBackground
        source={signBg}
        resizeMode='cover'
        style={styles.background}
      >
        <View style={styles.form}>
          <Title>Sign Up</Title>
          <View>
            <Input placeholder='Username' />
            <Input placeholder='Password' />
            <Button onPress={() => navigation.navigate("Conversation")}>
              Register
            </Button>
            <Footer
              text='Sign In'
              onPress={() => navigation.navigate("SignIn")}
            />
          </View>
        </View>
      </ImageBackground>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  background: {
    height: "100%",
    justifyContent: "center",
    paddingHorizontal: 20,
  },

  form: {
    width: "100%",
    backgroundColor: "#fff",
    paddingHorizontal: 25,
    paddingVertical: 50,
    borderRadius: 30,
  },
});

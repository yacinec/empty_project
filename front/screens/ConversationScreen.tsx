import React from "react";
import { StyleSheet, ScrollView } from "react-native";
import ConversationScreenCard from "../components/ConversationCard";

export default function ConversationScreenScreen() {
  return (
    <ScrollView style={styles.container}>
      <ConversationScreenCard
        name='Toad'
        image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fcdn.vox-cdn.com%2Fthumbor%2FxWkrJATSPVEJy6BiiYChC2tN_x0%3D%2F0x0%3A1200x1200%2F1200x800%2Ffilters%3Afocal(509x287%3A701x479)%2Fcdn.vox-cdn.com%2Fuploads%2Fchorus_image%2Fimage%2F61448237%2Ftum.0.jpg&f=1&nofb=1'
        lastMessage='Hello there!'
        date={new Date()}
      />
      <ConversationScreenCard
        name='Mario'
        image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.OXYCPOJpNfh2Elu5IEgrfAHaHX%26pid%3DApi&f=1'
        lastMessage="Let's go!"
        date={new Date()}
      />
      <ConversationScreenCard
        name='Luigi'
        lastMessage='Mamamia'
        image='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fpreview.redd.it%2Fq1huldiiihu21.png%3Fauto%3Dwebp%26s%3Dbe087134a0732577045647101cf3040fb622f0e8&f=1&nofb=1'
        date={new Date()}
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 70,
    paddingHorizontal: 50,
  },
});

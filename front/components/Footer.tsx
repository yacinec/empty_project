import React from "react";
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  GestureResponderEvent,
} from "react-native";

type FooterProps = {
  text: string;
  onPress: (event: GestureResponderEvent) => void;
};

const Footer: React.FC<FooterProps> = (props) => {
  return (
    <View style={styles.footer}>
      <Text style={styles.footerText}>Already have an account? </Text>
      <TouchableOpacity onPress={props.onPress}>
        <Text style={styles.footerBold}>{props.text}</Text>
      </TouchableOpacity>
    </View>
  );
};
export default Footer;

const styles = StyleSheet.create({
  footer: {
    marginTop: 30,
    justifyContent: "center",
    flexDirection: "row",
  },

  footerText: {
    textAlign: "center",
    fontSize: 15,
  },

  footerBold: {
    fontWeight: "700",
  },
});

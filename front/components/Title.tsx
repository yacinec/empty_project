import React from "react";
import { StyleSheet, Text } from "react-native";
import { COLORS } from "../types/constants";

type TitleProps = {};

const Title: React.FC<TitleProps> = (props) => {
  return <Text style={styles.title}>{props.children}</Text>;
};
export default Title;

const styles = StyleSheet.create({
  title: {
    fontSize: 30,
    marginBottom: 50,
    textAlign: "center",
    fontWeight: "700",
    color: COLORS.lightgrey,
  },
});

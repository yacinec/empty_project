import moment from "moment";
import React from "react";
import { StyleSheet, TouchableOpacity, View, Image, Text } from "react-native";

type ConversationCardProps = {
  image: string;
  name: string;
  lastMessage: string;
  date: Date;
};
const ConversationCard: React.FC<ConversationCardProps> = (props) => {
  return (
    <TouchableOpacity style={styles.conversation}>
      <View>
        <Image
          source={{
            uri: props.image,
          }}
          style={styles.conversationImage}
        ></Image>
      </View>
      <View style={styles.column}>
        <View style={styles.row}>
          <Text style={styles.conversationName}>{props.name}</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.conversationLastMessage}>
            {props.lastMessage}
          </Text>
          <Text style={styles.conversationDate}>
            {moment(props.date).format("h:mm a")}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ConversationCard;

const styles = StyleSheet.create({
  conversation: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 30,
    height: "auto",
  },

  column: {
    flex: 1,
    flexDirection: "column",
    paddingLeft: 10,
    justifyContent: "center",
  },

  row: {
    flexDirection: "row",
    justifyContent: "space-between",
  },

  conversationImage: {
    width: 50,
    height: 50,
    borderRadius: 30,
  },

  conversationName: {
    fontWeight: "700",
    fontSize: 15,
  },
  conversationLastMessage: {},
  conversationDate: {},
});

import React, { useState } from "react";
import { Text, TextInput, StyleSheet, View } from "react-native";
import { COLORS } from "../types/constants";
type InputProps = {
  placeholder: string;
};

const Input: React.FC<InputProps> = (props) => {
  const [isFocused, setIsFocused] = useState(false);

  return (
    <View
      style={[
        styles.inputBox,
        {
          borderColor: isFocused ? COLORS.darkblue : COLORS.lightgrey,
          borderWidth: isFocused ? 2 : 1,
        },
      ]}
    >
      <Text
        style={[
          styles.inputPlaceholder,
          {
            color: isFocused ? COLORS.darkblue : COLORS.lightgrey,
            fontWeight: isFocused ? "700" : "500",
          },
        ]}
      >
        {props.placeholder}
      </Text>
      <TextInput
        onBlur={() => setIsFocused(false)}
        onFocus={() => setIsFocused(true)}
        style={styles.inputText}
      />
    </View>
  );
};
export default Input;

const styles = StyleSheet.create({
  inputBox: {
    marginBottom: 30,
    borderRadius: 5,
  },

  inputPlaceholder: {
    alignSelf: "flex-start",
    paddingHorizontal: 3,
    backgroundColor: "#fff",
    position: "relative",
    top: -10,
    left: 15,
    fontSize: 15,
  },

  inputText: {
    fontSize: 18,
    paddingBottom: 20,
    paddingHorizontal: 15,
  },
});
